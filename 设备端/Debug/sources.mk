################################################################################
# 自动生成的文件。不要编辑！
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJS := 
SECONDARY_FLASH := 
SECONDARY_LIST := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
applications \
board \
libcpu/cpu \
libraries/hal_drivers \
libraries/hal_libraries/ab32vg1_hal/source \
libraries/hal_libraries/bmsis/source \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/atm \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/client \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_sign \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/mqtt/impl \
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/mqtt \
packages/ali-iotkit-v3.0.1/ports/rtthread \
packages/ali-iotkit-v3.0.1/ports \
packages/ali-iotkit-v3.0.1/samples/mqtt \
packages/at_device-v2.0.4/src \
packages/cJSON-v1.7.14 \
rt-thread/components/dfs/filesystems/devfs \
rt-thread/components/dfs/src \
rt-thread/components/drivers/misc \
rt-thread/components/drivers/rtc \
rt-thread/components/drivers/serial \
rt-thread/components/drivers/src \
rt-thread/components/finsh \
rt-thread/components/libc/compilers/common \
rt-thread/components/libc/compilers/newlib \
rt-thread/components/net/at/at_socket \
rt-thread/components/net/at/src \
rt-thread/components/net/netdev/src \
rt-thread/components/net/sal_socket/dfs_net \
rt-thread/components/net/sal_socket/impl \
rt-thread/components/net/sal_socket/socket \
rt-thread/components/net/sal_socket/src \
rt-thread/src \

