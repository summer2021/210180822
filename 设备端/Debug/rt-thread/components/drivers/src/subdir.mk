################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/components/drivers/src/completion.c \
../rt-thread/components/drivers/src/dataqueue.c \
../rt-thread/components/drivers/src/pipe.c \
../rt-thread/components/drivers/src/ringblk_buf.c \
../rt-thread/components/drivers/src/ringbuffer.c \
../rt-thread/components/drivers/src/waitqueue.c \
../rt-thread/components/drivers/src/workqueue.c 

OBJS += \
./rt-thread/components/drivers/src/completion.o \
./rt-thread/components/drivers/src/dataqueue.o \
./rt-thread/components/drivers/src/pipe.o \
./rt-thread/components/drivers/src/ringblk_buf.o \
./rt-thread/components/drivers/src/ringbuffer.o \
./rt-thread/components/drivers/src/waitqueue.o \
./rt-thread/components/drivers/src/workqueue.o 

C_DEPS += \
./rt-thread/components/drivers/src/completion.d \
./rt-thread/components/drivers/src/dataqueue.d \
./rt-thread/components/drivers/src/pipe.d \
./rt-thread/components/drivers/src/ringblk_buf.d \
./rt-thread/components/drivers/src/ringbuffer.d \
./rt-thread/components/drivers/src/waitqueue.d \
./rt-thread/components/drivers/src/workqueue.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/components/drivers/src/%.o: ../rt-thread/components/drivers/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\include\libc" -I"F:\code\rtt\ab32vg1\mqtt\mqtt" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\applications" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\board" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libcpu\cpu" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_drivers\config" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_drivers" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\ab32vg1_hal\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\ab32vg1_hal" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\bmsis\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\bmsis" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\atm" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\client" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\server" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_sign" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\infra" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt\impl" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\wrappers" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\at_device-v2.0.4\inc" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\cJSON-v1.7.14" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\dfs\filesystems\devfs" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\dfs\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\drivers\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\finsh" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\libc\compilers\common" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\libc\compilers\newlib" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\at\at_socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\at\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\netdev\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\impl" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\dfs_net" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\socket\sys_socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\include" -isystem"F:\code\rtt\ab32vg1\mqtt\mqtt" -include"F:\code\rtt\ab32vg1\mqtt\mqtt\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

