################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/src/clock.c \
../rt-thread/src/components.c \
../rt-thread/src/device.c \
../rt-thread/src/idle.c \
../rt-thread/src/ipc.c \
../rt-thread/src/irq.c \
../rt-thread/src/kservice.c \
../rt-thread/src/mem.c \
../rt-thread/src/memheap.c \
../rt-thread/src/mempool.c \
../rt-thread/src/object.c \
../rt-thread/src/scheduler.c \
../rt-thread/src/thread.c \
../rt-thread/src/timer.c 

OBJS += \
./rt-thread/src/clock.o \
./rt-thread/src/components.o \
./rt-thread/src/device.o \
./rt-thread/src/idle.o \
./rt-thread/src/ipc.o \
./rt-thread/src/irq.o \
./rt-thread/src/kservice.o \
./rt-thread/src/mem.o \
./rt-thread/src/memheap.o \
./rt-thread/src/mempool.o \
./rt-thread/src/object.o \
./rt-thread/src/scheduler.o \
./rt-thread/src/thread.o \
./rt-thread/src/timer.o 

C_DEPS += \
./rt-thread/src/clock.d \
./rt-thread/src/components.d \
./rt-thread/src/device.d \
./rt-thread/src/idle.d \
./rt-thread/src/ipc.d \
./rt-thread/src/irq.d \
./rt-thread/src/kservice.d \
./rt-thread/src/mem.d \
./rt-thread/src/memheap.d \
./rt-thread/src/mempool.d \
./rt-thread/src/object.d \
./rt-thread/src/scheduler.d \
./rt-thread/src/thread.d \
./rt-thread/src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/src/%.o: ../rt-thread/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\include\libc" -I"F:\code\rtt\ab32vg1\mqtt\mqtt" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\applications" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\board" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libcpu\cpu" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_drivers\config" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_drivers" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\ab32vg1_hal\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\ab32vg1_hal" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\bmsis\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\libraries\hal_libraries\bmsis" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\atm" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\client" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\server" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_sign" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\infra" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt\impl" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\ali-iotkit-v3.0.1\iotkit-embedded\wrappers" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\at_device-v2.0.4\inc" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\packages\cJSON-v1.7.14" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\dfs\filesystems\devfs" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\dfs\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\drivers\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\finsh" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\libc\compilers\common" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\libc\compilers\newlib" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\at\at_socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\at\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\netdev\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\impl" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\dfs_net" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\socket\sys_socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include\socket" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\components\net\sal_socket\include" -I"F:\code\rtt\ab32vg1\mqtt\mqtt\rt-thread\include" -isystem"F:\code\rtt\ab32vg1\mqtt\mqtt" -include"F:\code\rtt\ab32vg1\mqtt\mqtt\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

