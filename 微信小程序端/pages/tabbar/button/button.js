const host = 'https://hitwhnetcenter.cn/test'
const localhost = 'http://127.0.0.1:5000'
Page({
  data:{
    msg:"This is returned message",
    BatteryPercentage:"",
    LockSwitch:"关闭",
    GeoLocation:"",
    CurrentVoltage:"",
    propertylist:['GPIO1'],
    propertyindex:0,
    propertyvalueindex:0,
    propertyvaluelist:[0,1],
    property:"GPIO1",
    propertyvalue:0,
    warninfo:"",
    infotime:""

  },
  onLoad:function(options){
    this.GetCurrentVoltage()
    this.GetBatteryPercentage()
    this.GetLockSwitch()
    //this.GetGeoLocation()
    
    this.GetLBAEvent()

  },
  GetLBAEvent(){
    var that = this;
    var url = host+'/GetLBAEvent'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log("GetLBAEvent:")
        console.log(res.data)
        var name = res.data["Data"]["List"]["EventInfo"][0]["Name"]
        var time = parseInt(res.data["Data"]["List"]["EventInfo"][0]["Time"])
        
        var commonTime =new Date(time).toLocaleDateString().split('/').join('-')

        console.log(res.data["Data"]["List"]["EventInfo"][0]["Name"])
        console.log(res.data["Data"]["List"]["EventInfo"][0]["Time"])
        that.setData({
          warninfo:name,
          infotime:commonTime
        })
      }
    })
  },
  GetBatteryPercentage(){
    var that = this;
    var url = host+'/GetBatteryPercentage'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log("GetBatteryPercentage:")
        console.log(res.data)
        console.log(res.data["Data"]["List"]["PropertyInfo"][0]["Value"])
        that.setData({
          BatteryPercentage:res.data["Data"]["List"]["PropertyInfo"][0]["Value"]+"%"
        })
      }
    })

  },
  GetCurrentVoltage(){
    var that = this;
    var url = host+'/GetCurrentVoltage'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log("GetCurrentVoltage:")
        console.log(res.data)
        console.log(res.data["Data"]["List"]["PropertyInfo"][0]["Value"])
        var data = res.data["Data"]["List"]["PropertyInfo"][0]["Value"]
        that.setData({
          CurrentVoltage:data+"V",
          //BatteryPercentage:parseInt((data/7)*100)+"%"
        })
      }
    })

  },
  GetLockSwitch(){
    var that = this;
    var url = host+'/GetLockSwitch'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log("GetLockSwitch:")
        console.log(res.data)
        console.log(res.data["Data"]["List"]["PropertyInfo"][0]["Value"])
        var value=res.data["Data"]["List"]["PropertyInfo"][0]["Value"]
        if (value=="1"){
          that.setData({
            LockSwitch:"开启"
          })
        }
      }
    })
    

  },
  GetGeoLocation(){
    var that = this;
    var url = host+'/GetGeoLocation'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log("GetGeoLocation:")
        console.log(res.data)
        console.log(res.data["Data"]["List"]["PropertyInfo"][0]["Value"])
       
        that.setData({
          GeoLocation:res.data["Data"]["List"]["PropertyInfo"][0]["Value"]
        })
      }
    })

  },
  RegisterDevice(){
    var that = this;
    var url = host+'/RegisterDevice'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)
        })
      }
    })
  },
  QueryDeviceDetail(){
    var that = this;
    var url = host+'/QueryDeviceDetail'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)
        })
      }
    })
  },

  GetDeviceStatus(){
    var that = this;
    var url = host+'/GetDeviceStatus'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)
        })
      }
    })
  },

  QueryDevicePropertiesData(){
    var that = this;
    var url = host+'/QueryDevicePropertiesData'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)
        })
      }
    })
  },

  InvokeThingService(){
    var that = this;
    var url = host+'/InvokeThingService'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      method:'GET',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)+'\n'
        })
      }
    })
  },
 
  propertyChange: function(e) {

    var index = e.detail.value
    console.log('picker发送选择改变，携带下标为'+ e.detail.value)
    console.log('picker发送选择改变，携带值为'+this.data.propertylist[index])

    this.setData({
      propertyindex: e.detail.value,
    })

  },
  propertyvalueChange: function(e) {

    var index = e.detail.value 
    console.log('属性值携带下标为'+ e.detail.value)
    console.log('属性值选择改变，携带值为'+this.data.propertyvaluelist[index])

    this.setData({
      propertyvalueindex: e.detail.value,
    })

  },
  SetProperty(){
    var that = this;
    var url = host+'/SetProperty'
    wx.request({
      url: url,
      header:{
        'content-type':'application/json'
      },
      data:{
        property:this.data.propertylist[this.data.propertyindex],
        propertyvalue:this.data.propertyvaluelist[this.data.propertyvalueindex]
      },
      method:'POST',
      success(res){
        console.log(res.data)
        that.setData({
          msg:JSON.stringify(res.data)+'\n'+'说明:因为云端下发属性设置命令和设备收到并执行该命令是异步的，所以调用该接口时，返回的成功结果只表示云端下发属性设置的请求成功，不能保证设备端收到并执行了该请求。需设备端SDK成功响应云端设置设备属性值的请求，设备属性值才能真正设置成功'
        })
      }
    })

  },

  

})